#### A template startpage for your browser to help you access your stuff faster

![](docs/startpage.jpg)

## Requirements
To use this startpage project it is needed for you to have some knowledge in web coding, to customize it with your own thing.  
However, it is __handy__ for everyone who takes a look at the __documentation__.



# Coding
## Languages
* HTML
* CSS
* JS

## Credits
* jQuery
* Pignose Calendar
* Bootstrap
* Font Awesome 5 Free

## Modules (still working on it!)
Modules can be found in the /modules folder, such as :
* Google calendar API script
* PHP Database Tasks script
* Search field for search engine

The community can also develop their own and add it.